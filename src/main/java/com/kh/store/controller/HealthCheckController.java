package com.kh.store.controller;

import java.util.HashMap;
import java.util.List;

import com.kh.store.service.HealthCheckService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HealthCheckController {

    private final HealthCheckService healthCheckService;
    
    @GetMapping(value = "/healthCheck")
    public void healthCheck(){
        log.info("===헬스체크===");
    }

    @GetMapping(value = "/db/one/healthCheck/{product_seq}")
    public void healthDbOneCheck(@PathVariable("product_seq") String productSeq){
        log.info("===DB one 헬스체크===");
        HashMap<String, String> h = healthCheckService.healthDbOneCheck(productSeq);
        log.info("mybatis test :: "+h);
    }

    @GetMapping(value = "/db/list/healthCheck")
    public void healthDbListCheck(){
        log.info("===DB List 헬스체크===");
        List<HashMap<String, String>> h = healthCheckService.healthDbListCheck();
        log.info("mybatis test :: "+h);
    }
}