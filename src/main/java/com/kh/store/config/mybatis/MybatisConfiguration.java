package com.kh.store.config.mybatis;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Mybatis 설정
 *
 * @author kh
 */
@Configuration
@MapperScan(basePackages = {"com.kh.store.mapper"})
public class MybatisConfiguration {
    @Value("${db.datasource.driver-class-name}")
    private String driverClassName;
    
    @Value("${db.datasource.url}")
    private String url;
    
    @Value("${db.datasource.username}")
    private String username;
    
    @Value("${db.datasource.password}")
    private String password;

    @Autowired
    ApplicationContext applicationContext;
    
    /**
     * application.yml의 설정값을 읽어 dataSource 빈을 만든다.
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    /** 
     * DataSource를 이용하여 데이터베이스와 mybatis를 연결해준다.
     * 데이터베이스와의 연결과 SQL의 실행에 대한 모든 것을 가진 객체.
     */
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource) throws IOException {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        
        factoryBean.setDataSource(dataSource);
        factoryBean.setConfigLocation(applicationContext.getResource("classpath:/mybatis/mybatis-config.xml"));
        factoryBean.setMapperLocations(applicationContext.getResources("classpath:/mybatis/sql/**/*Mapper.xml"));
        return factoryBean;
    }

    /** 
     * mybatis의 쿼리문을 수행
     */
    @Bean
    public SqlSessionTemplate sqlSession(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}