package com.kh.store.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HealthCheckMapper {
    
    public List<HashMap<String, String>> healthDbListCheck();

	public HashMap<String, String> healthDbOneCheck(String productSeq);
}