package com.kh.store.service;

import java.util.HashMap;
import java.util.List;

import com.kh.store.mapper.HealthCheckMapper;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class HealthCheckService {

    private final HealthCheckMapper healthCheckMapper;

    public List<HashMap<String, String>> healthDbListCheck(){
        return healthCheckMapper.healthDbListCheck();
    }

    public HashMap<String, String> healthDbOneCheck(String productSeq) {
        return healthCheckMapper.healthDbOneCheck(productSeq);
    }
}