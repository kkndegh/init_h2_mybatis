DROP TABLE IF EXISTS member;
CREATE TABLE member
(
    member_seq IDENTITY PRIMARY KEY  COMMENT '회원 식별자',
    member_id VARCHAR(20) NOT NULL COMMENT '회원 아이디',
    member_name VARCHAR(20)  COMMENT '회원 이름',
    member_phone VARCHAR(15)  COMMENT '회원 핸드폰번호',
    member_del_yn VARCHAR(1)  COMMENT '회원 삭제여부',
    member_created_date TIMESTAMP  COMMENT '등록일',
    member_modified_date TIMESTAMP  COMMENT '수정일'
);
INSERT INTO member
( member_id, member_name, member_phone, member_del_yn, member_created_date )
VALUES
( 'HONG1', '홍길동', '010-2632-5301', 'N', CURRENT_TIMESTAMP() ),
( 'HONG2', '홍길동2', '010-1234-5302', 'N', CURRENT_TIMESTAMP() ),
( 'HONG3', '홍길동3', '010-2345-5303', 'N', CURRENT_TIMESTAMP() ),
( 'HONG4', '홍길동4', '010-5432-5304', 'N', CURRENT_TIMESTAMP() ),
( 'HONG5', '홍길동5', '010-1523-5305', 'N', CURRENT_TIMESTAMP() ),
( 'HONG6', '홍길동6', '010-6333-5306', 'N', CURRENT_TIMESTAMP() );

DROP TABLE IF EXISTS product;
CREATE TABLE product
(
    product_seq IDENTITY PRIMARY KEY  COMMENT '상품 식별자',
    product_id VARCHAR(20) NOT NULL COMMENT '상품 아이디',
    product_name VARCHAR(20)  COMMENT '상품 이름',
    product_del_yn VARCHAR(1)  COMMENT '상품 삭제여부',
    product_created_date TIMESTAMP  COMMENT '등록일',
    product_modified_date TIMESTAMP  COMMENT '수정일'
);
INSERT INTO product
( product_id, product_name, product_del_yn, product_created_date )
VALUES
( 'prd1', '상품1', 'N', CURRENT_TIMESTAMP() ),
( 'prd2', '상품2', 'N', CURRENT_TIMESTAMP() ),
( 'prd3', '상품3', 'N', CURRENT_TIMESTAMP() ),
( 'prd4', '상품4', 'N', CURRENT_TIMESTAMP() ),
( 'prd5', '상품5', 'N', CURRENT_TIMESTAMP() ),
( 'prd6', '상품6', 'N', CURRENT_TIMESTAMP() );